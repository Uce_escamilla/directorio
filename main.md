# Directorio SDC-Scrumers Development Community

## Carla Paola
* [Bitbucket](https://bitbucket.org/CarlaPerez23/)
	* [MiniProyectos](https://bitbucket.org/CarlaPerez23/mini-proyectos/) (Último commit - 2017-03-11) (3 Retos)
		* [https://bitbucket.org/CarlaPerez23/mini-proyectos.git](https://bitbucket.org/CarlaPerez23/mini-proyectos.git)
* [GitLab](https://gitlab.com/RahkeidDragneel97) (Sin repositorios)

## Mario Adrian 
* [Bitbucket](https://bitbucket.org/mario_gm/)
	* [ScrumRetos](https://bitbucket.org/mario_gm/scrumretos) (Último commit - 2017-03-13) (3 Retos)
		* [https://bitbucket.org/mario_gm/scrumretos.git](https://bitbucket.org/mario_gm/scrumretos.git)
* [GitLab](https://gitlab.com/mario_gm) (Sin repositorios)

## Miguel Ángel Palomar
* [Bitbucket](https://bitbucket.org/Piiman/)
	* [SDC](https://bitbucket.org/Piiman/sdc) (Último commit - 2017-03-04) (1 Reto)
		* [https://bitbucket.org/Piiman/sdc.git](https://bitbucket.org/Piiman/sdc.git)
* [GitLab](https://gitlab.com/Piiman2)
	* [SDC](https://gitlab.com/Piiman2/SDC) (Último commit - 2 weeks ago) (2 Retos)
		* [https://gitlab.com/Piiman2/SDC.git](https://gitlab.com/Piiman2/SDC.git)

## Jesús Hernandez
* [Bitbucket](https://bitbucket.org/jesushc_unam/)
	* [Reto2](https://bitbucket.org/jesushc_unam/reto2) (Último commit - 2017-03-10)
		* [https://bitbucket.org/jesushc_unam/reto2.git](https://bitbucket.org/jesushc_unam/reto2.git)
* [GitLab](https://gitlab.com/jesushc)
	* [Reto1JHC](https://gitlab.com/jesushc/reto1JHC) (Último commit - 2 weeks ago)
		* [https://gitlab.com/jesushc/reto1JHC.git](https://gitlab.com/jesushc/reto1JHC.git)

## Dialid Cerón
* [Bitbucket](https://bitbucket.org/Zokakiller)
	* [RetoII](https://bitbucket.org/Zokakiller/reto-ii) (Último commit - 2017-03-11) (1 Reto)
		* [https://bitbucket.org/Zokakiller/reto-ii.git](https://bitbucket.org/Zokakiller/reto-ii.git)
* [GitLab](https://gitlab.com/Zokakiller) (Sin repositorios)

## Aaron Cervantez
* [Bitbucket](https://bitbucket.org/SkrillexMau/) (Sin repositorios)
* [GitLab](https://gitlab.com/skrillexmau) (Sin repositorios)

## Antonio Molina
* [Bitbucket](https://bitbucket.org/webingenieria/) (Sin repositorios)
* [GitLab](https://gitlab.com/webingenieria) (Sin repositorios)

## Ariadne Olarte
* [Bitbucket](https://bitbucket.org/aragondb/) (Sin repositorios)
* [GitLab](https://gitlab.com/aragondb12) (Sin repositorios)

## Edgar Morales
* [Bitbucket](https://bitbucket.org/emp76/) (Sin repositorios)
* [GitLab](https://gitlab.com/emp76) (Sin repositorios)

## Javier Beltrán
* [Bitbucket](https://bitbucket.org/Javier_Darko/) (Sin repositorios)
* [GitLab](https://gitlab.com/Javier_Darko) (Sin repositorios)

## Marco Antonio Martell
* [Bitbucket](https://bitbucket.org/marcoHdezMartell/) (Sin repositorios)
* [GitLab](https://gitlab.com/marcoHdezMartell) (Sin repositorios)

## Mario Alberto
* [Bitbucket](https://bitbucket.org/Mario97ba/) (Sin repositorios)
* [GitLab](https://gitlab.com/mario97) (Sin repositorios)

## Hector Omar
* [Bitbucket](https://bitbucket.org/OmarRm/) (Sin repositorios)
* [GitLab](https://gitlab.com/OmarRm)
	* [Retos](https://gitlab.com/OmarRm/retos) (Vacío)
	* [Scrum](https://gitlab.com/OmarRm/scrum) (Vacío)

## Juan Carlos Camacho
* [Bitbucket](https://bitbucket.org/hgjuancar21/) (Sin repositorios)


## Réne Dávila
* [Bitbucket](https://bitbucket.org/RenEigner) (Sin repositorios)
* [GitLab](https://gitlab.com/ren.ico.0930) (Sin repositorios)

## Richar Espinosa
* [Bitbucket](https://bitbucket.org/eljr1294/) (Sin repositorios)
* [GitLab](https://gitlab.com/eljr12) (Sin repositorios)

## Victor Manuel
* [Bitbucket](https://bitbucket.org/HMikado/) (Sin repositorios)
* [GitLab](https://gitlab.com/Mikado9523) (Sin repositorios)


